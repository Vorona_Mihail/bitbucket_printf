/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_undefined_behaviour.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/27 13:13:53 by mvorona           #+#    #+#             */
/*   Updated: 2017/03/06 17:48:15 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	ft_fill_struct_str_undefbeh(char **traverse, t_defarg *des_arg)
{
	int		i;
	char	*traverse_head;
	int		b;

	i = 0;
	b = 0;
	traverse_head = *traverse;
	while ((ft_isdigit(**traverse) == 1 || ft_strchr("# .-h+ljz_", **traverse) != NULL) && **traverse != '\0')
	{
		i++;
		(*traverse)++;
	}
	if (**traverse == '\0')
		return (0);
	des_arg->conv = **traverse;
	des_arg->arg_str = ft_strnew(1);
	ft_memset(des_arg->arg_str, **traverse, 1);
	des_arg->str = ft_strnew(i + 2);
	while (b <= i)
		des_arg->str[b++] = *(traverse_head++);
	des_arg->str[b] = '\0';
	return (1);
}

static void	ft_fill_flags_undefbeh(t_defarg *des_arg)
{
	ft_zero_flag(des_arg);
	ft_minus_flag(des_arg);
}

void		ft_undefined_behaviour(char **traverse, va_list arg, t_defarg *des_arg)
{
	if (ft_fill_struct_str_undefbeh(traverse, des_arg) == 1)
	{
		ft_fill_flags_undefbeh(des_arg);
		ft_fill_width(des_arg);
		ft_unbehaviour_print(des_arg);
	}
	ft_free_struct(des_arg);
}
