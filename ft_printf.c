/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/16 13:09:14 by mvorona           #+#    #+#             */
/*   Updated: 2017/03/06 18:29:42 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static char	*ft_check_double_percent(char *traverse)
{
	int		i;
	int		percent_counter;

	i = 0;
	percent_counter = 0;
	while ((traverse[i] == '%' || traverse[i] == ' '))
	{
		if (traverse[i] == '%')
			percent_counter++;
		if (percent_counter == 2)
			break ;
		i++;
	}
	if (percent_counter == 2)
		return (NULL);
	else
		return (traverse);
}

static char	*ft_write_doble_percent(char *traverse)
{
	int		percent_counter;

	percent_counter = 0;
	while ((*traverse == '%' || *traverse == ' '))
	{
		if (*traverse++ == '%')
			percent_counter++;
		if (percent_counter == 2)
			break ;
	}
	if (percent_counter == 2)
		ft_putchar_g('%');
	return (traverse);
}

static void	ft_utility_pf(char **traverse, va_list arg)
{
	while (**traverse != '\0')
	{
		while (**traverse != '\0')
		{
			if (**traverse == '%')
			{
				if (ft_check_double_percent(*traverse) == NULL)
					*traverse = ft_write_doble_percent(*traverse);
				else
					break ;
			}
			else
				ft_putchar_g(*(*traverse)++);
		}
		if (*((*traverse) + 1) == '\0' || **traverse == '\0')
			break ;
		(*traverse)++;
		pf_head(traverse, arg);
		if (**traverse == '\0')
			break ;
		(*traverse)++;
	}
}

int			ft_printf(char *argum, ...)
{
	char	*traverse;
	va_list	arg;

	pf_return = 0;
	va_start(arg, argum);
	traverse = argum;
	ft_utility_pf(&traverse, arg);
	va_end(arg);
	return (pf_return);
}
