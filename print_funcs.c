/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_funcs.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/19 14:50:12 by mvorona           #+#    #+#             */
/*   Updated: 2017/03/06 17:52:45 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void		ft_print_width(char symbol, char *str_print, t_defarg *des_arg)
{
	int i;

	i = des_arg->wid - ft_strlen(str_print);
	while (i > 0)
	{
		ft_putchar_g(symbol);
		i--;
	}
}

static void		ft_utility_print_str(t_defarg *des_arg, char *str_print)
{
	if (des_arg->wid > (int)ft_strlen(str_print))
	{
		if (des_arg->flag_minus == 1)
		{
			ft_putstr_g(str_print);
			ft_print_width(' ', str_print, des_arg);
			return ;
		}
		else
		{
			if (des_arg->flag_zero == 1)
				ft_print_width('0', str_print, des_arg);
			else
				ft_print_width(' ', str_print, des_arg);
		}
	}
	ft_putstr_g(str_print);
}

void			ft_str_print_actions(t_defarg *des_arg)
{
	char	*str_print;

	if (des_arg->prec >= 0 || des_arg->wid >= 0)
	{
		if (des_arg->prec < (int)ft_strlen(des_arg->arg_str) && des_arg->prec >= 0)
			str_print = ft_strsub(des_arg->arg_str, 0, (size_t)des_arg->prec);
		else
			str_print = ft_strdup(des_arg->arg_str);
		ft_utility_print_str(des_arg, str_print);
		ft_strdel(&str_print);
	}
	else
		ft_putstr_g(des_arg->arg_str);
}

void			ft_chr_print_actions(t_defarg *des_arg)
{
	if (des_arg->flag_minus == 1)
	{
		if (des_arg->arg_str[0] == '\0')
			ft_putchar('\0');
		else
			ft_putstr_g(des_arg->arg_str);
	}
	while (des_arg->wid > 1)
	{
		if (des_arg->flag_zero == 0 || des_arg->flag_minus == 1)
			ft_putchar_g(' ');
		else if (des_arg->flag_zero == 1)
			ft_putchar_g('0');
		des_arg->wid--;
	}
	if (des_arg->flag_minus == 0)
	{
		if (des_arg->arg_str[0] == '\0')
			ft_putchar('\0');
		else
		ft_putstr_g(des_arg->arg_str);
	}
}

void			ft_print_arg(t_defarg *des_arg)
{
	if (ft_strchr("DdiuUcsoOxXcs", des_arg->conv)
		&& ft_strlen(des_arg->str) == 1)
		ft_putstr_g(des_arg->arg_str);
	else if (des_arg->conv == 'S' || des_arg->conv == 's')
	ft_str_print_actions(des_arg);
	else if (ft_strchr("cC", des_arg->conv))
		ft_chr_print_actions(des_arg);
	else if (ft_strchr("dDioOuUxX", des_arg->conv))
		ft_digital_print_actions(des_arg);
}
