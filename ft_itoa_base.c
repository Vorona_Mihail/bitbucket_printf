/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/03 14:46:04 by mvorona           #+#    #+#             */
/*   Updated: 2017/03/02 14:27:47 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static unsigned int		ft_len_and_digit(long long num, int base,
										unsigned long long *digit)
{
	unsigned int 		i;
	unsigned long long	tmp;

	i = 0;
	if (num < 0 && base == 10)
	{
		i++;
		*digit = (unsigned long long)num * -1;
		tmp = (unsigned long long)num * -1;
	}
	else
	{
		*digit = (unsigned long long)num;
		tmp = (long long)num;
	}
	if (tmp == 0)
		i++;
	while (tmp > 0)
	{
		i++;
		tmp /= (unsigned long long)base;
	}
	return (i);
}

static char				*ft_make_str(char *str, unsigned int i,
										unsigned long long digit, int base)
{
	char	*represent;

	represent = "0123456789ABCDEF";
	while (digit > 0)
	{
		str[i--] = represent[digit % (unsigned long long)base];
		digit /= (unsigned long long)base;
	}
	return (str);
}

char					*ft_itoa_base(long long num, int base)
{
	char					*str_num;
	unsigned int			i;
	unsigned long long		digit;

	if (base < 2 && base > 16)
		return (NULL);
	i = ft_len_and_digit(num, base, &digit);
	if ((str_num = (char*)malloc(sizeof(char) * i + 1)))
	{
		str_num[i--] = '\0';
		if (num == 0)
			str_num[i] = '0';
		else
			str_num = ft_make_str(str_num, i, digit, base);
	}
	if (num < 0)
		str_num[0] = '-';
	return (str_num);
}
