/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pf_head.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/16 13:19:43 by mvorona           #+#    #+#             */
/*   Updated: 2017/03/06 18:13:16 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** В данной функции проверяем есть ли спецификатор в строке, если попадаем 
** на "%" или конец строки, то тогда считаем, что спецификатор типа не найден, иначе найден и функция возвращает "1"
*/

static int	check_conversion(char *traverse)
{
	while (ft_strchr("sSpdDioOuUxXcC", *traverse) == NULL)
	{
		if ((*traverse == '%' || *traverse == '\0')
			|| (ft_isalpha(*traverse) && ft_strchr("hljz", *traverse) == NULL))
			return (0);
		traverse++;
	}
	if (ft_strchr("sSpdDioOuUxXcC", *traverse) && *traverse != '\0')
		return (1);
	return (0);
}
/*
static int	chek_undef(char *traverse)
{
	while (*traverse != '%' || *traverse != '\0')
	{
		if (ft_strchr("hljz+- .#", *traverse) != NULL && !ft_isdigit(*traverse))
			return (1);
		traverse++;
	}
	return (0);
}
*/
/*
** С этой функции начинается разветвление работы основной функции. Сначала инициализируем структуру,
** потом приравниваем все ее элементы к нулю, чтобы туда не записался мусор. Далее проверяем, существует ли спецификаток для первого аргумента/строки в целом. Если существует, то идем по ветке первой
** после уcловного оператора функции, если спецификатор типа не определен идем по альтернативной ветке
*/
void	pf_head(char **traverse, va_list arg)
{
	t_defarg *des_arg;

	des_arg = (t_defarg*)malloc(sizeof(t_defarg));
	ft_init_struct_elem_to_zero(des_arg);
	if (check_conversion(*traverse) == 1)
		ft_conversion_head(traverse, arg, des_arg);
	else
		ft_undefined_behaviour(traverse, arg, des_arg);
	free(des_arg);
//	ft_free_struct(des_arg);
}
