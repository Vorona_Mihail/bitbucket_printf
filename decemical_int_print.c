/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   decemical_int_print.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/20 16:02:37 by mvorona           #+#    #+#             */
/*   Updated: 2017/03/03 16:25:21 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_pos_decemic_minus_print(t_defarg *des_arg)
{
	char	*dig_print;
	
	dig_print = ft_pos_minus_util_arr(des_arg);
	if (des_arg->prec <= 0)
		ft_print_plus_space(des_arg);	
	ft_putstr_g(dig_print);
	while (des_arg->wid > (int)ft_strlen(dig_print))
	{
		ft_putchar_g(' ');
		des_arg->wid--;
	}
	ft_strdel(&dig_print);
}

void	ft_pos_decemic_print(t_defarg *des_arg)
{
	char	*dig_print;

	dig_print = ft_pos_util_arr(des_arg);
	if (des_arg->prec > 0 || (des_arg->flag_zero == 0 && des_arg->prec > 0))
	{
		while (des_arg->wid > (int)ft_strlen(dig_print))
		{
			ft_putchar_g(' ');
			des_arg->wid--;
		}
	}
	else
	{
		if (des_arg->flag_zero == 1 || (des_arg->prec <= 0 && des_arg->flag_minus == 1))
			ft_print_plus_space(des_arg);
		if (des_arg->flag_space == 1 || des_arg->flag_plus == 1)
			des_arg->wid--;
		while (des_arg->wid > (int)ft_strlen(dig_print))
			ft_print_zero_space(des_arg);	
	}
	if (des_arg->prec == 0 && des_arg->arg_str[0] == '0')
	{
		if (des_arg->wid > 0)
			ft_putchar_g(' ');
	}
	else
		ft_putstr_g(dig_print);
	ft_strdel(&dig_print);
}

void	ft_neg_decemic_print_minus(t_defarg *des_arg)
{
	char	*dig_print;

	dig_print = ft_neg_minus_util_arr(des_arg);
	ft_putstr_g(dig_print);
	while (des_arg->wid > (int)ft_strlen(dig_print))
	{
		ft_putchar_g(' ');
		des_arg->wid--;
	}
	ft_strdel(&dig_print);
}

void	ft_neg_decemic_print(t_defarg *des_arg)
{
	char	*dig_print;

	if (des_arg->prec < (int)ft_strlen(des_arg->arg_str) && des_arg->flag_zero == 0)//= (int)ft_strlen(des_arg->arg_str))
		dig_print = ft_strdup(des_arg->arg_str);
	else	
		dig_print = ft_neg_util_arr(des_arg);
	if (des_arg->flag_zero == 0)
	{
		while (des_arg->wid > (int)ft_strlen(dig_print))
		{
			ft_putchar_g(' ');
			des_arg->wid--;
		}
	}
	else if (des_arg->flag_zero == 1)
	{
//		if (dig_print[0] != '-' && des_arg->arg_str[0] == '-')
//		{
			ft_putchar_g('-');
			des_arg->wid--;
//		}
		while (des_arg->wid > (int)ft_strlen(dig_print))
		{
			ft_putchar_g('0');
			des_arg->wid--;
		}
	}
	ft_putstr_g(dig_print);
	ft_strdel(&dig_print);
}

void	ft_decemic_int_print_actions(t_defarg *des_arg)
{
	if (des_arg->prec > 0 || des_arg->flag_minus == 1)
		des_arg->flag_zero = 0;
	if (des_arg->arg_str[0] != '-')
	{
		if (des_arg->flag_plus == 1)
			des_arg->flag_space = 0;
		if (des_arg->flag_minus == 1)
			ft_pos_decemic_minus_print(des_arg);
		else if (des_arg->prec <= 0 && des_arg->flag_zero == 0
				&& (des_arg->flag_plus == 1
				|| des_arg->flag_space == 1)
				&& des_arg->flag_minus == 0)
			ft_pos_decemic_plus_space_print(des_arg);
		else
			ft_pos_decemic_print(des_arg);
	}
	else
	{
		if (des_arg->flag_minus == 1)
			ft_neg_decemic_print_minus(des_arg);
		else
			ft_neg_decemic_print(des_arg);
	}
}
