/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_util_arr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/23 18:33:56 by mvorona           #+#    #+#             */
/*   Updated: 2017/02/26 20:35:17 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*ft_neg_util_arr(t_defarg *des_arg)
{
	char	*dig_print;
	char	*tmp;
	int		diff;

	if (!(des_arg->prec < 0 && des_arg->flag_zero == 0)) 
	{
		tmp = des_arg->arg_str;
		des_arg->arg_str = ft_strsub(des_arg->arg_str, 1,
											ft_strlen(des_arg->arg_str) - 1);
		free(tmp);
	}
	if ((diff = des_arg->prec - (int)ft_strlen(des_arg->arg_str) + 1) > 0)
	{
		dig_print = ft_strnew(diff);
		ft_memset(dig_print, '0', diff);
		dig_print[0] = '-';
		tmp = dig_print;
		dig_print = ft_strjoin(dig_print, des_arg->arg_str);
		free(tmp);
	}
	else
		dig_print = ft_strdup(des_arg->arg_str);
	return (dig_print);
}

char	*ft_neg_minus_util_arr(t_defarg *des_arg)
{
	char	*dig_print;
	char	*tmp;
	int		diff;

	if (des_arg->prec > 0)
	{
		tmp = des_arg->arg_str;
		des_arg->arg_str = ft_strsub(des_arg->arg_str, 1,																			ft_strlen(des_arg->arg_str) - 1);
		free(tmp);
	}
	if ((diff = des_arg->prec - (int)ft_strlen(des_arg->arg_str) + 1) > 0)
	{
		dig_print = ft_strnew(diff);
		ft_memset(dig_print, '0', diff);
		dig_print[0] = '-';
		tmp = dig_print;
		dig_print = ft_strjoin(dig_print, des_arg->arg_str);
		free(tmp);
	}
	else
		dig_print = ft_strdup(des_arg->arg_str);
	return (dig_print);
}

char	*ft_pos_minus_util_arr(t_defarg *des_arg)
{
	char	*dig_print;
	char	*tmp;
	int		diff;
	int		flag;

	flag = 0;
	if (des_arg->flag_plus == 1 || des_arg->flag_space == 1)
		flag = 1;
	if ((diff = des_arg->prec - (int)ft_strlen(des_arg->arg_str) + flag) >= 0)
	{
		dig_print = ft_strnew(diff);
		ft_memset(dig_print, '0', diff);
		if (des_arg->flag_plus == 1)
			dig_print[0] = '+';
		else if (des_arg->flag_space == 1)
			dig_print[0] = ' ';
		tmp = dig_print;
		dig_print = ft_strjoin(dig_print, des_arg->arg_str);
		free(tmp);
	}
	else
		dig_print = ft_strdup(des_arg->arg_str);
	return (dig_print);
}

char	*ft_pos_util_arr(t_defarg *des_arg)
{
	int 	flag;
	char	*dig_print;
	char	*tmp;
	int		diff;


	flag = 0;
	if (des_arg->flag_plus == 1 || des_arg->flag_space == 1)
		flag = 1;
	if (des_arg->flag_zero == 1)
		flag = 0;
	if ((diff = des_arg->prec - (int)ft_strlen(des_arg->arg_str) + flag) >= 0)
	{
		dig_print = ft_strnew(diff);
		ft_memset(dig_print, '0', diff);
		if (des_arg->flag_plus == 1 && des_arg->flag_zero == 0)
			dig_print[0] = '+';
		else if (des_arg->flag_space == 1 && des_arg->flag_zero == 0)
			dig_print[0] = ' ';
		tmp = dig_print;
		dig_print = ft_strjoin(dig_print, des_arg->arg_str);
		free(tmp);
	}
	else
		dig_print = ft_strdup(des_arg->arg_str);
	return (dig_print);
}
