/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   conversion_actions.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/17 20:09:30 by mvorona           #+#    #+#             */
/*   Updated: 2017/02/28 20:35:44 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_conversion_actions_head(va_list arg, t_defarg *des_arg)
{
	if (ft_strchr("DdioOuUxX", des_arg->conv))
		ft_check_write_arg_str(arg, des_arg);
	else if (ft_strchr("sScC", des_arg->conv))
		ft_put_chr_arg_in_str_head(arg, des_arg);
	else if (ft_strchr("p", des_arg->conv))
		ft_pointer_address_func(arg, des_arg);
}
