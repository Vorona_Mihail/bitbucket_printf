/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   extra_funcs.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/03 19:31:29 by mvorona           #+#    #+#             */
/*   Updated: 2017/03/04 17:23:26 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		ft_putchar_g(char c)
{
	write(1, &c, 1);
	pf_return++;
}

void		ft_putstr_g(char const *s)
{
	int	i;

	i = 0;
	if (s)
	{
		while (s[i] != '\0')
		{
			write(1, &s[i], 1);
			i++;
			pf_return++;
		}
	}
}

void		ft_putnbr_uns(unsigned int n)
{
	if (n > 9)
		ft_putnbr_uns(n / 10);
	ft_putchar((n % 10) + '0');
}

char	*ft_tolowstr(char *str)
{
	int	i;

	i = 0;
	while (str[i] != '\0')
	{
		if (ft_isalpha(str[i]))
			str[i] = ft_tolower(str[i]);
		i++;
	}
	return (str);
}

t_defarg	*ft_init_struct_elem_to_zero(t_defarg *des_arg)
{
	des_arg->str = NULL;
	des_arg->conv = 0;
	des_arg->modif = NULL;
	des_arg->wid = -1;
	des_arg->prec = -1;
	des_arg->flag_zero = 0;
	des_arg->flag_sharp = 0;
	des_arg->flag_space = 0;
	des_arg->flag_minus = 0;
	des_arg->flag_plus = 0;
	des_arg->arg_str = NULL;
	des_arg->l_string = NULL;
	return (des_arg);
}
