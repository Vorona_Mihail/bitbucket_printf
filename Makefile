# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: egaragul <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/02/28 18:18:25 by egaragul          #+#    #+#              #
#    Updated: 2017/03/06 18:02:20 by mvorona          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #
 
NAME = libftprintf.a

SRC = conversion_actions.c\
	  conversion_head.c\
	  decemical_int_print.c\
	  extra_funcs.c\
	  fill_modificators.c\
	  flags_funcs.c\
	  ft_itoa_base.c\
	  ft_itoa_base_unsigned.c\
	  ft_printf.c\
	  ft_util_arr.c\
	  digital_print_actions.c\
	  pf_head.c\
	  precision_width_fill.c\
	  print_funcs.c\
	  write_digital_conv_in_struct_argum.c\
	  write_str_chr_argum_int_str.c\
	  ft_pos_decemic_plus_space_print.c\
	  ft_print_plus_space.c\
	  ft_pointer_adress_func.c\
	  ft_unbehaviour_print.c\
	  ft_undefined_behaviour.c\
	  ft_free_struct.c\
	  ft_strdup_wint.c\

GG = $(SRC:.c=.o)
HEAD = ft_printf.h
FLAG = -c -Wall -Wextra -Werror

all: $(NAME)

$(NAME): $(GG)
	make -C libft
	mv libft/libft.a ./$(NAME)
	ar rc $(NAME) $(GG)
	ranlib $(NAME)

%.o: %c $(HEAD)
	gcc -o $(NAME) $(FLAG) $@ $<

clean:
	rm -f $(GG)
	make clean -C libft/

fclean: clean
	rm -f $(NAME)
	make fclean -C libft

re: fclean all
