/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free_struct.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/28 14:25:08 by mvorona           #+#    #+#             */
/*   Updated: 2017/03/04 17:10:23 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_free_struct(t_defarg *des_arg)
{
	if (des_arg->str)
		ft_strdel(&des_arg->str);
	if (des_arg->arg_str)
		ft_strdel(&des_arg->arg_str);
	if (des_arg->modif)
		ft_strdel(&des_arg->modif);
}
