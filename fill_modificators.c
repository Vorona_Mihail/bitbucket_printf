/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_modificators.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/19 13:38:18 by mvorona           #+#    #+#             */
/*   Updated: 2017/02/22 22:14:41 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** Вспомогательная функция для выявления модификатора "h", который может 
** находится в подстроке из нечетного колличества символов 'h'
**
*/
static int			ft_detect_h(t_defarg *des_arg)
{
	int i;
	int	count_h;

	i = 0;
	while (des_arg->str[i] != '\0')
	{
		count_h = 0;
		while (des_arg->str[i] == 'h')
		{
			count_h++;
			i++;
		}
		if (count_h % 2 != 0)
			return (1);
		i++;
	}
	return (0);
}
/*
** Ищем по значимости (от более важных к менее важным) модификаторы и 
** записываем их в обьект структуры "modif".
*/
static t_defarg		*ft_fill_digital_modif(t_defarg *des_arg)
{
	if (ft_strchr(des_arg->str, 'z'))
		des_arg->modif = ft_strdup("z");
	else if (ft_strchr(des_arg->str, 'j'))
		des_arg->modif = ft_strdup("j");
	else if (ft_strstr(des_arg->str, "ll"))
		des_arg->modif = ft_strdup("ll");
	else if (ft_strchr(des_arg->str, 'l') || ft_strchr("DOU", des_arg->conv))
		des_arg->modif = ft_strdup("l");
	else if (ft_strchr(des_arg->str, 'h') && ft_detect_h(des_arg) == 1)
		des_arg->modif = ft_strdup("h");
	else if (ft_strstr(des_arg->str, "hh"))
		des_arg->modif = ft_strdup("hh");
	return (des_arg);
}
/*
** В этой функции мы разделяем поиск цифровых и сивольных/строчных спецификаторов
** потому что к "cs" применяется только "l", а остальные учитываются для 
** цифровых. Если спецификатор типа одиз из "DOU", то сразу записываем "l", как модификатор.
*/
static t_defarg		*ft_check_modificator_group(t_defarg *des_arg)
{
//	if (ft_strchr("DOU", des_arg->conv))
//		des_arg->modif = ft_memset((void*)ft_strnew(1), 'l', 1);
	if (ft_strchr("diouxXDOU", des_arg->conv) != NULL)
		ft_fill_digital_modif(des_arg);
	else if (ft_strchr("csCS", des_arg->conv) != NULL)
	{
		if (ft_strchr(des_arg->str, 'l') || ft_strchr("CS", des_arg->conv))
			des_arg->modif = ft_strdup("l");
	}
	return (des_arg);
}
/*
** Проходим позаписанной нами строке и если встречаем какой-либо из символов
** "hljz", то значит есть модификатор. Также учитываем, что "DOU" это уже
** и есть "ld" "lu" "lo", по этому мы в обьект структуры "modif" мы запишем
** модификатор "l" в данном случае.
*/
t_defarg			*ft_fill_modificators(t_defarg *des_arg)
{
	int i;

	i = 0;
	while (des_arg->str[i] != '\0')
	{
		if (ft_strchr("hljzDOUCS", des_arg->str[i]))
		{
			ft_check_modificator_group(des_arg);
			return (des_arg);
		}
		i++;
	}
	return (des_arg);
}
