/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wordcount.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/07 14:43:51 by mvorona           #+#    #+#             */
/*   Updated: 2016/12/12 22:15:37 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

#define IN 1
#define OUT 0

int		ft_wordcount(char *str, char c)
{
	int	nw;
	int state;
	int	i;

	nw = 0;
	i = 0;
	state = OUT;
	while (str[i] != '\0')
	{
		if (str[i] == c)
			state = OUT;
		else if (state == OUT)
		{
			state = IN;
			nw++;
		}
		i++;
	}
	return (nw);
}
