/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/02 16:39:07 by mvorona           #+#    #+#             */
/*   Updated: 2016/12/07 14:09:31 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_eleminstr(const char *s, char c)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (s[i] != '\0')
	{
		if (s[i] == c)
			j++;
		i++;
	}
	return (j);
}

static char	**ft_make_arr(char const *s, char **new_arr, char c)
{
	size_t	var[4];

	var[0] = 0;
	var[2] = 0;
	var[1] = 0;
	while (s[var[0]] != '\0')
	{
		var[3] = 0;
		if (s[var[0]] != c)
		{
			var[3] = 1;
			var[1] = var[0];
		}
		while (s[var[1]] != c && s[var[1]] != '\0')
			var[1]++;
		if (var[3] == 1)
		{
			new_arr[var[2]++] = ft_strsub(s,
					(unsigned int)var[0], var[1] - var[0]);
			var[0] = var[1] - 1;
		}
		var[0]++;
	}
	new_arr[var[2]] = NULL;
	return (new_arr);
}

char		**ft_strsplit(char const *s, char c)
{
	char	**new_arr;

	if (!s || !c)
		return (NULL);
	else
	{
		if ((new_arr = (char**)malloc(sizeof(char*)
						* (ft_strlen(s) - ft_eleminstr(s, c) + 1))))
		{
			return (ft_make_arr(s, new_arr, c));
		}
		else
			return (NULL);
	}
}
