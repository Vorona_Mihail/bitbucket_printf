/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 18:55:38 by mvorona           #+#    #+#             */
/*   Updated: 2016/11/30 19:23:58 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memalloc(size_t size)
{
	size_t	i;
	void	*fresh_mem;

	i = 0;
	if ((fresh_mem = malloc(size)))
	{
		while (i < size)
		{
			*((char*)fresh_mem + i) = 0;
			i++;
		}
		return (fresh_mem);
	}
	else
		return (NULL);
}
