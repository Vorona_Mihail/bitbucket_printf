/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 20:11:50 by mvorona           #+#    #+#             */
/*   Updated: 2016/12/07 14:09:54 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnew(size_t size)
{
	char		*new_arr;
	size_t		i;

	i = 0;
	new_arr = (char*)malloc(sizeof(char) * (size + 1));
	if (!new_arr)
		return (NULL);
	while (i < size)
		new_arr[i++] = '\0';
	new_arr[i] = '\0';
	return (new_arr);
}
