/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 20:10:39 by mvorona           #+#    #+#             */
/*   Updated: 2016/12/05 18:47:18 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *big, const char *little)
{
	int i;
	int j;

	i = 0;
	if (little[0] == '\0')
		return ((char*)big);
	else
	{
		while (big[i] != '\0')
		{
			j = 0;
			while (big[i + j] == little[j])
			{
				if (little[j + 1] == '\0')
					return ((char*)(big + i));
				j++;
			}
			i++;
		}
	}
	return (NULL);
}
