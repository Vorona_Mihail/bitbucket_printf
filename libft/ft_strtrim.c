/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/01 18:03:53 by mvorona           #+#    #+#             */
/*   Updated: 2016/12/05 18:12:15 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	ft_len(char const *s, size_t len)
{
	size_t	i;

	i = len;
	while (s[i - 1] == ' ' || s[i - 1] == '\t' || s[i - 1] == '\n')
	{
		i--;
		len--;
	}
	if (len > 0)
	{
		i = 0;
		while (s[i] == ' ' || s[i] == '\t' || s[i] == '\n')
		{
			i++;
			len--;
		}
	}
	return (len);
}

char			*ft_strtrim(char const *s)
{
	size_t	i;
	size_t	j;
	char	*new_arr;

	i = 0;
	j = 0;
	if (s)
	{
		if ((new_arr = (char*)malloc(sizeof(char)
						* (ft_len(s, ft_strlen(s)) + 1))))
		{
			while (i < ft_len(s, ft_strlen(s)))
			{
				while ((s[j] == ' ' || s[j] == '\t' || s[j] == '\n')
						&& i == 0)
					j++;
				new_arr[i++] = s[j++];
			}
			new_arr[i] = '\0';
			return (new_arr);
		}
		return (NULL);
	}
	return (NULL);
}
