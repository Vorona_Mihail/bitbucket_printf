/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_size.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/07 11:15:17 by mvorona           #+#    #+#             */
/*   Updated: 2016/12/07 12:55:03 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

unsigned int	ft_lst_size(t_list *lst)
{
	unsigned int	i;
	t_list			*new_list;

	i = 0;
	new_list = lst;
	while (lst)
	{
		new_list = new_list->next;
		i++;
	}
	return (i);
}
