/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 20:59:34 by mvorona           #+#    #+#             */
/*   Updated: 2016/12/06 21:10:36 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	char	*new_arr;
	size_t	i;

	i = 0;
	if (s && f)
	{
		new_arr = (char*)malloc(sizeof(*s) * (ft_strlen(s) + 1));
		if (new_arr)
		{
			while (i < ft_strlen(s))
			{
				new_arr[i] = f(s[i]);
				i++;
			}
			new_arr[i] = '\0';
			return (new_arr);
		}
		else
			return (NULL);
	}
	else
		return (NULL);
}
