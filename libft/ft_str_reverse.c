/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_reverse.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/07 11:23:25 by mvorona           #+#    #+#             */
/*   Updated: 2016/12/07 11:44:17 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_str_reverse(char *str)
{
	unsigned int	i;
	unsigned int	j;
	char			buf;

	i = 0;
	j = 0;
	if (!str)
		return (NULL);
	while (str[i] != '\0')
		i++;
	while (j < (i / 2))
	{
		buf = str[i - 1];
		str[i - 1] = str[j];
		str[j] = buf;
		j++;
	}
	return (str);
}
