/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 21:19:38 by mvorona           #+#    #+#             */
/*   Updated: 2016/12/06 18:51:32 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char				*new_arr;
	unsigned int		i;

	i = 0;
	if (s)
	{
		new_arr = (char*)malloc(sizeof(*s) * (ft_strlen(s) + 1));
		if (new_arr)
		{
			while (i < ft_strlen(s))
			{
				new_arr[i] = f(i, s[i]);
				i++;
			}
			new_arr[i] = '\0';
			return (new_arr);
		}
		else
			return (NULL);
	}
	else
		return (NULL);
}
