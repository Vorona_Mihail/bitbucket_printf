/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/06 16:02:46 by mvorona           #+#    #+#             */
/*   Updated: 2016/12/06 17:11:42 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*new_list;

	if (!lst || !f)
		return (NULL);
	if (!((new_list = (t_list*)malloc(sizeof(t_list)))))
		return (NULL);
	new_list = f(lst);
	if (lst->next)
		new_list->next = ft_lstmap(lst->next, f);
	return (new_list);
}
