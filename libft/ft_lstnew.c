/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/05 18:50:34 by mvorona           #+#    #+#             */
/*   Updated: 2016/12/06 13:50:04 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*new_list;

	if ((new_list = (t_list*)malloc(sizeof(t_list))))
	{
		if (content == NULL)
		{
			new_list->content = NULL;
			new_list->content_size = 0;
		}
		else
		{
			if (!(new_list->content = ft_memalloc(content_size)))
				free(new_list->content);
			else
			{
				ft_memcpy(new_list->content, content, content_size);
				new_list->content_size = content_size;
			}
		}
		new_list->next = NULL;
		return (new_list);
	}
	else
		return (NULL);
}
