/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_intarr_revsort.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/07 13:56:32 by mvorona           #+#    #+#             */
/*   Updated: 2016/12/07 14:10:16 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		*ft_intarr_revsort(int *arr, int len)
{
	int	i;
	int	j;
	int	buf;

	if (!arr)
		return (0);
	i = 0;
	while (i < len)
	{
		j = i + 1;
		while (j < len)
		{
			if (arr[i] < arr[j])
			{
				buf = arr[i];
				arr[i] = arr[j];
				arr[j] = buf;
			}
			j++;
		}
		i++;
	}
	return (arr);
}
