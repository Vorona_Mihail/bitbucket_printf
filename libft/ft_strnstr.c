/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/29 20:45:04 by mvorona           #+#    #+#             */
/*   Updated: 2016/12/06 21:26:02 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *big, const char *lit, size_t len)
{
	size_t	i;
	size_t	j;

	i = 0;
	if (lit[0] == '\0')
		return ((char*)big);
	else
	{
		while (big[i] != '\0')
		{
			j = 0;
			while (big[i + j] == lit[j] && (i + j < len))
			{
				if (lit[j + 1] == '\0')
					return ((char*)(big + i));
				j++;
			}
			i++;
		}
	}
	return (NULL);
}
